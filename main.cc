#include <iostream>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include "EpollInstance.hh"
#include "EpollStdIn.hh"
#include "EpollTimer.hh"

using namespace std;

int
main(void)
{
    EpollInstance ep;
    EpollTimer tim1(1000);
    EpollTimer tim2(1500);
    EpollStdIn sin;

    ep.registerEpollEntry(tim1);
    ep.registerEpollEntry(tim2);
    ep.registerEpollEntry(sin);

    while (1) {
        ep.waitAndHandleEvents();
    }

    ep.unregisterEpollEntry(tim1);
    ep.unregisterEpollEntry(tim2);
    ep.unregisterEpollEntry(sin);

    return 0;
}
